# SimplifiedSolovayStrassen

Code to produce the demos in my presentation on the Simplified Solovay-Strassen algorithm for the Seminar Randomized Algorithms.

## Requirements

To run the code in this repository you need the gmp library, which enables the use of multiple precision integers.

You can find further information on [the website](https://gmplib.org/) including an [installation guide](https://gmplib.org/manual/Installing-GMP) for Unix systems.

## Demos

### Demo 1

The code in demo1.c lets you test specific candidate primes for the number of witnesses they have, test specific candidate witnesses, and compare the SSSA to a number of other tests.

Command line options:

 * -p : to specify the candidate prime (required)
 * -v / -V : levels of verbosity about the witnesses found
 * -w / -W : both allow you to specify a witness you would like to examine in detail (differ in level of detail)
 * -c : allows you to specify a second test you would like to compare against (options[only requires first letter]: FLT / Solovay Strassen (General) / Miller Rabin)

 To compile the code use __make demo1__ and run it with any arguments you want to try, to get a version of the demos on the slides execute __demo1\<x\>.sh__

 ### Demo 2

 The code in demo2.c simply runs a Simplified Solovay Strassen test on the given candidate prime.

 Command line options:

 * -p : to specify the candidate prime (required)
 * -v / -V : levels of verbosity
 * -d : fixes the random seeds used to make results reproducible
 * -r : lets you specify the number of repetitions you want

 To compile the code use __make demo2__ and run it with any arguments you want to try, to get a version of the demos on the slides execute __demo2\<x\>.sh__