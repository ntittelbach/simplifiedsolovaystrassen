make cleanall -s
make demo1 -s

echo "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo "Example for Carmichael Number 561\n"

./demo1 -p 561 -v -c FLT
./demo1 -p 561 -W 3 -c FLT
./demo1 -p 561 -W 5 -c FLT
./demo1 -p 561 -W 8 -c FLT

echo "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
echo "Example for Carmichael Number 1105\n"

./demo1 -p 1105 -v -c FLT

echo "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"