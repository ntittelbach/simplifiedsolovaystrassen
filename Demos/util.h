#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>
#include <stddef.h>
#include "primeTest.h"

extern gmp_randstate_t randstate;
extern uint32_t bits;

void powmod_v(mpz_t res, mpz_t base, mpz_t exp, mpz_t mod, uint8_t verbose);

extern uint64_t rand_large_32();

extern void get_witness(mpz_t ret, const mpz_t possible_prime);

#endif //UTIL_H
