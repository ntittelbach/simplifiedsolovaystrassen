#include <stdio.h>
#include <stdbool.h>
#include <getopt.h>
#include <stdlib.h>
#include <time.h>
#include <gmp.h>

#include "primeTest.h"
#include "util.h"

uint32_t bits = 1536;
gmp_randstate_t randstate;

int main(int argc, char** argv) {

    // read options and initialize
    uint8_t verbose = 0;
    bool deterministic = 0;
    uint32_t reps = 50;

    mpz_t candidate; mpz_init(candidate);

    char *options = "p:vVdr:";

    for(int o = getopt(argc, argv, options); o >= 0; o = getopt(argc, argv, options)){
        switch(o){
            case 'p':
                mpz_set_str(candidate, optarg, 10);
                break;
            case 'r':
                reps = atoi(optarg);
            case 'v':
                verbose = 1;
                break;
            case 'V':
                verbose = 2;
                break;
            case 'd':
                deterministic = 1;
                break;
            case '?':
                printf("illegal option\n");
                break;
            case ':':
                printf("missing argument\n");
        }
    }

    gmp_randinit_default(randstate);
    if(deterministic) {
        srand(42);
        gmp_randseed_ui(randstate, 1984);
    }else{
        srand(time(0));
        gmp_randseed_ui(randstate, time(0));
    }


    mpz_t candidates[reps];
    bool isWitness[reps];
    uint32_t cWit = 0;


    for(uint32_t i = 0; i<reps; ++i){
        mpz_init(candidates[i]);
        get_witness(candidates[i], candidate);
        testRecord_t rec = prime_testSolovayStrassenSimp(candidate, candidates[i]);
        isWitness[i] = !rec.isPossiblePrime;
        if(isWitness[i]) cWit++;
        clearTRec(rec);
    }

    //output
    gmp_printf(" => %Zd %s\n", candidate, cWit?"is not prime":"is likely prime");
    printf("\tFound %i/%i witnesses", cWit, reps);
    if(verbose==1 && cWit){
        printf(":\n\t[");
        for(uint32_t i = 0; i<reps; i++){ if(isWitness[i]) gmp_printf("%Zd, ", candidates[i]);}
        printf("\b\b]");
    }
    printf("\n");
    if(verbose>=2){
        for(uint32_t i = 0; i<reps; i++){
            gmp_printf("%Zd %s \n", candidates[i], isWitness[i]?"is a Witness":"is not a Witness");
        }
        printf("\n");
    }
    printf("\n");

    mpz_clears(candidate, NULL);
    for(uint32_t i = 0; i<reps; ++i){
        mpz_clear(candidates[i]);
    }

    

}
