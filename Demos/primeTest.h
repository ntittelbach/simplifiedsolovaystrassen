//
// Created by arche on 3/1/2022.
//

#ifndef PRIMETEST_H
#define PRIMETEST_H

#define NAME_NAIVE ("Naive Algo")
#define NAME_GCD ("Gcd Cert")
#define NAME_Fermat ("Fermat Cert")
#define NAME_SOLOSTRSIMP ("Simp Solovay Strassen Cert")
#define NAME_SOLOSTR ("Solovay Strassen Cert")
#define NAME_MILLERRABIN ("Miller Rabin Cert")

#include <stdint.h>
#include <stdbool.h>
#include <gmp.h>

// TODO implement message

struct test_record{
    bool isPossiblePrime;
    mpz_t witness;
    char* message;
};

typedef struct test_record testRecord_t;

typedef testRecord_t (*prime_test) (const mpz_t candidate, const mpz_t witness);

extern bool prime_testNaiveDet(const mpz_t candidate);

extern testRecord_t prime_testNaiveRand(const mpz_t candidate, const mpz_t witness);

extern testRecord_t prime_testGcd(const mpz_t candidate, const mpz_t witness);

extern testRecord_t prime_testFermat(const mpz_t candidate, const mpz_t witness);

extern testRecord_t prime_testSolovayStrassenSimp(const mpz_t candidate, const mpz_t witness);

extern testRecord_t prime_testSolovayStrassen(const mpz_t candidate, const mpz_t witness);

extern testRecord_t prime_testMillerRabin(const mpz_t candidate, const mpz_t witness);

extern bool prime_testRep(const mpz_t candidate, prime_test test, uint32_t reps);

extern void clearTRec(testRecord_t rec);



#endif //PRIMETEST_H
