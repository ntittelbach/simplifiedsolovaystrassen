#include "util.h"
#include <stdlib.h>
#include <stdio.h>



void get_witness(mpz_t ret, const mpz_t possible_prime){
    do{
        mpz_urandomm(ret, randstate, possible_prime);
    }while(mpz_cmp_ui(ret, 0) == 0);
    return;
}


void powmod_v(mpz_t res, mpz_t base, mpz_t exp, mpz_t mod, uint8_t verbose){
    if(mpz_cmp_ui(exp, 0)==0){
        mpz_set_ui(res, 1);
    }else if(mpz_even_p(exp)){
        mpz_t tmp1, tmp2; mpz_inits(tmp1, tmp2, NULL);
        mpz_divexact_ui(tmp1, exp, 2);
        powmod_v(tmp2, base, tmp1, mod, verbose);
        mpz_powm_ui(res, tmp2, 2, mod);
        mpz_clears(tmp1, tmp2, NULL);
    }else{
        mpz_t tmp1, tmp2, tmp3; mpz_inits(tmp1, tmp2, tmp3, NULL);
        mpz_sub_ui(tmp1, exp, 1);
        powmod_v(tmp2, base, tmp1, mod, verbose);
        mpz_mul(tmp3, tmp2, base);
        mpz_mod(res, tmp3, mod);
        mpz_clears(tmp1, tmp2, tmp3, NULL);
    }
    gmp_printf("%Zd^%Zd mod %Zd: %Zd\n", base, exp, mod, res);
}
