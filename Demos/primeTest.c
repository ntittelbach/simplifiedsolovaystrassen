//
// Created by arche on 3/1/2022.
//

#include "primeTest.h"
#include "util.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>




bool prime_testNaiveDet(const mpz_t candidate){
    mpz_t root;
    mpz_init(root);
    mpz_sqrt(root, candidate);

    mpz_t i;
    mpz_init_set_ui(i, 0);
    for(; mpz_cmp(i,root) < 0; mpz_add_ui(i, i, 1)){
        if(mpz_divisible_p(candidate, i))
            return 0;
    }
    return 1;
}


testRecord_t prime_testNaiveRand(const mpz_t candidate, const mpz_t witness){
    testRecord_t rec = { ! (mpz_divisible_p(candidate, witness))};
    mpz_init_set(rec.witness, witness);
    return rec;
}


testRecord_t prime_testGcd(const mpz_t candidate, const mpz_t witness){
    mpz_t gcd;
    mpz_init(gcd);
    mpz_gcd(gcd, witness, candidate);
    testRecord_t rec = {mpz_cmp_ui(gcd, 1)==0 };
    mpz_init_set(rec.witness, witness);

    mpz_clear(gcd);

    return rec;
}


testRecord_t prime_testFermat(const mpz_t candidate, const mpz_t witness){
    mpz_t pow, order;
    mpz_inits(pow, order, NULL);

    mpz_sub_ui(order, candidate, 1);
    mpz_powm(pow, witness, order, candidate);
    testRecord_t rec = {mpz_cmp_ui(pow, 1)==0};
    mpz_init_set(rec.witness, witness);

    mpz_clears(pow, order, NULL);

    return rec;
}


testRecord_t prime_testSolovayStrassenSimp(const mpz_t candidate, const mpz_t witness){
    // create record skeleton
    testRecord_t rec = {0};
    mpz_init_set(rec.witness, witness);


    // calculate witness^(possPrime-1 / 2) mod possPrime
    mpz_t pow, exp, order;
    mpz_inits(pow, exp, order, NULL);

    mpz_sub_ui(order, candidate, 1);
    mpz_divexact_ui(exp, order, 2);

    mpz_powm(pow, witness, exp, candidate);

    rec.isPossiblePrime = (mpz_cmp_ui(pow, 1)==0 || mpz_cmp(pow, order)==0);

    mpz_clears(pow, exp, order, NULL);
    return rec;
}


testRecord_t prime_testSolovayStrassen(const mpz_t candidate, const mpz_t witness){
    // create record skeleton
    testRecord_t rec = {0};
    mpz_init_set(rec.witness, witness);


    // check condition gcd(possPrime, witness) = 1
    mpz_t gcd;
    mpz_init(gcd);
    mpz_gcd(gcd, witness, candidate);

    if(!(mpz_cmp_ui(gcd, 1)==0)) {
        mpz_clear(gcd);
        return rec;
    }

    // calculate witness^(possPrime-1 / 2) mod possPrime AND Jacobi(witness / possPrime)
    mpz_t pow, exp, order;
    mpz_inits(pow, exp, order, NULL);

    mpz_sub_ui(order, candidate, 1);
    mpz_divexact_ui(exp, order, 2);

    mpz_powm(pow, witness, exp, candidate);

    int jac = mpz_jacobi(witness, candidate);

    rec.isPossiblePrime = jac > 0 ? (mpz_cmp_ui(pow, 1)==0) : (mpz_cmp(pow, order)==0);

    mpz_clears(pow, exp, order, gcd, NULL);
    return rec;
}


testRecord_t prime_testMillerRabin(const mpz_t candidate, const mpz_t witness){
    // create record skeleton
    testRecord_t rec = {1};
    mpz_init_set(rec.witness, witness);

    mpz_t order, d, pow;
    uint64_t r = 0;
    mpz_inits(order, pow, NULL);
    mpz_sub_ui(order, candidate, 1);
    mpz_init_set(d, order);


    while(mpz_even_p(d)){
        ++r;
        mpz_divexact_ui(d, d, 2);
    }

    mpz_powm(pow, witness, d, candidate);

    if(mpz_cmp_ui(pow, 1)==0 || mpz_cmp(pow, order)==0){
        mpz_clears(order, pow, d, NULL);
        return rec;
    }
    for(uint64_t i = 1; i<r; ++i){
        mpz_powm_ui(pow, pow, 2, candidate);
        if(mpz_cmp(pow, order)==0){
            mpz_clears(order, pow, d, NULL);
            return rec;
        }
    }

    mpz_clears(order, pow, d, NULL);

    rec.isPossiblePrime = false;
    return rec;
}


bool prime_testRep(const mpz_t candidate, prime_test test, uint32_t reps){
    mpz_t witness;
    mpz_init(witness);
    for(uint32_t i = 0; i<reps; ++i){
        get_witness(witness, candidate);

        testRecord_t rec = test(candidate, witness);
        if(!rec.isPossiblePrime){
            clearTRec(rec);
            mpz_clear(witness);
            return false;
        }
        clearTRec(rec);

    }
    mpz_clear(witness);
    return true;
}


void clearTRec(testRecord_t rec){
    mpz_clear(rec.witness);
}