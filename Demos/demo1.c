#include <stdio.h>
#include <stdbool.h>
#include <getopt.h>
#include <stdlib.h>
#include <time.h>
#include <gmp.h>

#include "primeTest.h"
#include "util.h"

uint32_t bits = 1536;
gmp_randstate_t randstate;


int main(int argc, char** argv) {

    // read options and initialize
    uint8_t verbose = 0;
    bool deterministic = 0;
    uint8_t witnesses = 0;

    prime_test compareTest = NULL;
    char* compareName = NULL;

    char *options = "p:vdw:W:c:";

    mpz_t candidate, witness;
    mpz_inits(candidate, witness, NULL);

    for(int o = getopt(argc, argv, options); o >= 0; o = getopt(argc, argv, options)){
        switch(o){
            case 'p':
                mpz_set_str(candidate, optarg, 10);
                break;
            case 'w':
                witnesses = 1;
                mpz_set_str(witness, optarg, 10);
                break;
            case 'W':
                witnesses = 2;
                mpz_set_str(witness, optarg, 10);
                break;
            case 'v':
                verbose = 1;
                break;
            case 'd':
                deterministic = 1;
                break;
            case 'c':
                compareName = optarg;
                switch(optarg[0]){
                    case 'S':
                        compareTest = prime_testSolovayStrassen;
                        break;
                    case 'F':
                        compareTest = prime_testFermat;
                        break;
                    case 'M':
                        compareTest = prime_testMillerRabin;
                        break;
                }
                if(!compareTest) exit(-1);
                break;
            case '?':
                printf("illegal option\n");
                break;
            case ':':
                printf("missing argument\n");
        }
    }

    gmp_randinit_default(randstate);
    if(deterministic) {
        srand(42);
        gmp_randseed_ui(randstate, 1984);
    }else{
        srand(time(0));
        gmp_randseed_ui(randstate, time(0));
    }

    if(mpz_cmp_ui(candidate, 0)<=0){
        fprintf(stderr, "Missing or Illegal Argument: -p (a candidate prime to test)");
    }

    // count number of witnesses
    mpz_t cWitS, cWitC, i; mpz_inits(cWitS, cWitC, NULL);
    for (mpz_set_ui(i, 1); mpz_cmp(i, candidate) < 0; mpz_add_ui(i, i, 1)) {
        testRecord_t tRec1 = prime_testSolovayStrassenSimp(candidate, i);
        if(!tRec1.isPossiblePrime) mpz_add_ui(cWitS, cWitS, 1);

        testRecord_t tRec2;
        if(compareTest){
                tRec2 = compareTest(candidate, i);
                if(!tRec2.isPossiblePrime) mpz_add_ui(cWitC, cWitC, 1);
        }
        
        //output
        if(verbose>=2) gmp_printf("%Zd is a Witness for: FLT:%i", i, !tRec1.isPossiblePrime);
        if(verbose>=2 && compareTest) gmp_printf(" | Comparison[%s]:%i", compareName, !tRec2.isPossiblePrime);
        if(verbose>=2)  printf("\n");
        clearTRec(tRec1);
        if(compareTest)
            clearTRec(tRec2);
    }
    mpz_clear(i);

    //calculate ratios
    mpf_t card, ratioS, ratioC, cWitSF, cWitCF;
    mpf_inits(card, ratioS, ratioC, cWitSF, cWitCF, NULL);
    mpf_set_z(cWitSF, cWitS); mpf_set_z(cWitCF, cWitC); mpf_set_z(card, candidate); mpf_sub_ui(card, card, 1);
    mpf_div(ratioS, cWitSF, card);
    mpf_div(ratioC, cWitCF, card);
    //output
    if(verbose>=1) {
        gmp_printf(" => Candidate Prime %Zd: \n", candidate);
        gmp_printf("\tSSSA: %Zd Witnesses | Fraction of %.5Ff\n", cWitS, ratioS);
        if(compareTest)
            gmp_printf("\tComparison[%s]: %Zd Witnesses | Fraction of %.5Ff\n\n", compareName, cWitC, ratioC);
    }

    // test specific witnesses
    if(witnesses) {
        gmp_printf("Testing with candidate witness %Zd:\n\n", witness);

        mpz_t res, power, cm1; mpz_inits(res, power, cm1, NULL);mpz_sub_ui(cm1, candidate, 1); mpz_div_ui(power, cm1, 2);
        if(witnesses>1){
            powmod_v(res, witness, cm1, candidate, witnesses);
            printf("\n");
        }
        testRecord_t tRecSSSA = prime_testSolovayStrassenSimp(candidate, witness);
        gmp_printf("%Zd %s for SSSA\n", witness, !tRecSSSA.isPossiblePrime? "Is a Witness": "Is not a Witness");
        clearTRec(tRecSSSA);
        if (compareTest){
            testRecord_t tRecComp = compareTest(candidate, witness);
            gmp_printf("%Zd %s for Comparison Test [%s]\n", witness, !tRecComp.isPossiblePrime? "Is a Witness": "Is not a Witness", compareName);
            clearTRec(tRecComp);
        }
        printf("\n");

        mpz_clears(res, power, cm1, NULL);
    }


    mpz_clears(witness, cWitS, cWitC, NULL);
    mpf_clears(cWitSF, cWitCF, ratioS, ratioC,  card, NULL);
    
    mpz_clear(candidate);
    
}
